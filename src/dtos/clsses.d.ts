declare interface ClassesProps {
  classes: {
    [k: string]: string
  }
  style?: React.CSSProperties
  classNames? : string,
}