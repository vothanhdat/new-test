import { hot } from 'react-hot-loader'
import React from 'react'
import { BrowserRouter as Router, Route, Link, withRouter, Switch } from "react-router-dom";
import TermsPage from '../pages/terms'
import PrivacyPage from '../pages/privacy'
import BlogPage from '../pages/medium/blog'
import BlogPost from '../pages/medium/post'
import TestForm from '../pages/test/testform'
import TestStreams from '../pages/uplive/hot'
import TestStreamsTags from '../pages/uplive/hotTags'
import Paralax from '../pages/paralax/doc'
import Flickr from '../pages/flickr'
import FlickrPhoto from '../pages/flickr/photo'
import FlickrUser from '../pages/flickr/user'
import Live from '../pages/uplive/live'
import { withTranslate } from '../components/Language';
import { TransitionGroup, CSSTransition } from 'react-transition-group';
import withSCSS from 'withsass.macro';
import AlbumContainer from '../pages/flickr/album';




const RouteIndex = withTranslate(
  ({ language: { name: langname }, location , history}) => {
    const prefix = langname == 'en' ? '/' : '/' + langname + '/'
    return <div className={`router-action-${history.action}`}>
      
      {/* <Route exact path={prefix + ''} component={TestStreams} />
      <Route exact path={prefix + 'hot'} component={TestStreams} />
      <Route exact path={prefix + 'live/:uid'} component={Live} />
      <Route exact path={prefix + 'live/:uid/:roomid'} component={Live} />
      <Route exact path={prefix + 'hot/:tags'} component={TestStreamsTags} /> */}

      
{/* 
      <Route exact path={prefix + "termsofuse/"} component={TermsPage} />
      <Route exact path={prefix + "privacypolicy/"} component={PrivacyPage} />
      <Route exact path={prefix + "blog/"} component={BlogPage} />
      <Route exact path={prefix + "blog/:postid/"} component={BlogPost} />
      <Route exact path={prefix + 'testform'} component={TestForm} />
      <Route exact path={prefix + 'paralax'} component={Paralax} />
      {/* <Switch location={location}>
      </Switch> */} 
      <TransitionGroup>
        <CSSTransition key={location.pathname} classNames='fade' timeout={{ enter: 300, exit: 300 }}>
          <Switch location={location}>
            <Route exact path={prefix + 'flickr'} component={Flickr} />
            <Route exact path={prefix + 'flickr/p/:photoid'} component={FlickrPhoto} />
            <Route path={prefix + 'flickr/u/:userid'} component={FlickrUser} />
            <Route path={prefix + 'flickr/a/:albumid'} component={AlbumContainer} />
          </Switch>
         </CSSTransition>
      </TransitionGroup>
    </div>
  }
)

export default hot(module)(withRouter(withSCSS('./index.scss')(RouteIndex)));
// export default (RouteIndex);

